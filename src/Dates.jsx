import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import { InputGroup, InputGroupText, InputGroupAddon, Input } from "reactstrap";
import { DateTime } from "luxon";


import {
  formatDate,
  diferencia,
  quantsDies,
} from "./Helpers";

import { Espai } from "./StyleHelpers";

export default class Dates extends Component {
  constructor(props) {
    super(props);
    console.log("dentro del constructor");
    this.state = {
      data1: new Date(),
      data2: new Date(),
    };
  }

  onChangeDate(date1, date2) {
    if (date1.getTime() < date2.getTime()) {
      this.setState({ data1: date1, data2: date2 });
    } else {
      this.setState({ data1: date2, data2: date1 });
    }
  }

  render() {

    const intervaloDias = diferencia(this.state.data1, this.state.data2) + " dias";
  
    const diumenges = quantsDies(this.state.data1, this.state.data2, 0);
    const dissabtes = quantsDies(this.state.data1, this.state.data2, 6);

    const luxon1 = DateTime.fromJSDate(this.state.data1);
    const luxon2 = DateTime.fromJSDate(this.state.data2);

    const interval = luxon2.diff(luxon1, [
      "weeks",
      "days",
      "hours",
      "minutes",
      "seconds",
      "milliseconds",
    ]);

    return (
      <>
        <Container>
        <h3>Componente de clase</h3>
          <Espai />
          <Row>
            <Col xs="4">
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Inici</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="date"
                  value={formatDate(this.state.data1)}
                  onChange={(e) =>
                    this.onChangeDate(
                      new Date(e.target.value),
                      this.state.data2
                    )
                  }
                />
              </InputGroup>
              <br />
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>Final</InputGroupText>
                </InputGroupAddon>
                <Input
                  type="date"
                  value={formatDate(this.state.data2)}
                  onChange={(e) =>
                    this.onChangeDate(
                      this.state.data1,
                      new Date(e.target.value)
                    )
                  }
                />
              </InputGroup>
            </Col>
          </Row>
          <Espai />
          <Row>
            <Col>
              <h3>Data1: {this.state.data1.toISOString()}</h3>
              <h3>Data2: {this.state.data2.toISOString()}</h3>
              <br />
              <h3>{intervaloDias}</h3>
 

              <hr />
              <h4>Inici {luxon1.toISO()}</h4>
              <h4>Final {luxon2.toISODate()}</h4>
              <h4>
                {interval.weeks} setmanes - {interval.days} dies -{" "}
                {interval.hours} hores - {interval.minutes} minuts{" "}
              </h4>

              <hr />

              <h4>Diumenges: {diumenges}</h4>
              <h4>Dissabtes: {dissabtes}</h4>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}
