
import Dates from './Dates';
import DatesF from './DatesF';


const App =  () => {

  return (
    <>
      <Dates />
      <DatesF />
 
    </>
  )

}

export default App;