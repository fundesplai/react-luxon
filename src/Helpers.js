

// recibe objeto Date de JS
// devuelve string en formato 'YYYY-MM-DD'
function formatDate(date) {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear();
  
    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;
  
    return [year, month, day].join("-");
  }


// pure function
function diferencia(data1, data2) {
    const difMs = data2.getTime() - data1.getTime();
    const dias = difMs/1000/60/60/24;
    return dias;
}

function sumaDias(data1, dies){
    const data1Ms = data1.getTime();
    const diaMs = 24*60*60*1000;
    const data2Ms = data1Ms + (dies * diaMs);
    const data2 = new Date(data2Ms);

    return data2;
}

const diasSemana = ["domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"];


function quantsDies(data1, data2, diaSetmana){
    const unDia = 24*60*60*1000;
    let msInici = data1.getTime();
    let msFinal = data2.getTime();
    if (msInici>msFinal){
        [msInici, msFinal] = [msFinal, msInici];
    }
    let msBucle = msInici;
    let diesX=0;
    do {
        const dataBucle = new Date(msBucle);
        if (dataBucle.getDay()===diaSetmana){
            diesX++;
        }
        msBucle += unDia;
    } while (msBucle<msFinal);
    
    const dataFinal = new Date(msFinal);
    if (dataFinal.getDay()===diaSetmana){
        diesX++;
    }
    return diesX;
}



  export { formatDate, diferencia, sumaDias, diasSemana, quantsDies };
