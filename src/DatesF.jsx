import React, { useState, useEffect } from "react";

import DateTimePicker from "react-datetime-picker";
import { Container, Row, Col } from "reactstrap";
import { InputGroup, InputGroupText, InputGroupAddon, Input } from "reactstrap";

import {  sumaDias } from "./Helpers";
import { Espai } from "./StyleHelpers";

import { DateTime } from "luxon";


const DatesF = () => {
  const [data1, setData1] = useState(new Date());
  const [data2, setData2] = useState(sumaDias(new Date(), 7));

  useEffect(() => {
    console.log("funcional: useeffect único, después de 1r render");
    return () => {
      console.log("componente datesF se va a cerrar");
    };
  }, []);

  useEffect(() => {
    console.log("funcional: useeffect despues de cada render");
  });

  useEffect(() => {
    if (data1>data2){
        setData2(data1);
        setData1(data2);
    }
  }, [data1, data2]);

  console.log("functional: entramos a return ");

  const luxon1 = DateTime.fromJSDate(data1);
  const luxon2 = DateTime.fromJSDate(data2);

  const interval = luxon2.diff(luxon1, [
    "weeks",
    "days",
    "hours",
    "minutes",
    "seconds",
    "milliseconds",
  ]);

  return (
    <>
 
      <Container>
      <Espai />
      <h3>Componente funcional</h3>
        <Espai />
        <Row>
          <Col xs="4">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <InputGroupText>Inici</InputGroupText>
              </InputGroupAddon>
              <DateTimePicker
                onChange={setData1}
                value={data1}
                maxDetail="second"
              />
            </InputGroup>
            <br />
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <InputGroupText>Final</InputGroupText>
              </InputGroupAddon>
              <DateTimePicker
                onChange={setData2}
                value={data2}
                maxDetail="second"
              />
            </InputGroup>
          </Col>
        </Row>
        <Espai />
        <Row>
          <Col>
            <h3>Data1: {data1.toISOString()}</h3>
            <h3>Data2: {data2.toISOString()}</h3>
            <h4>
                {interval.weeks} setmanes - {interval.days} dies -{" "}
                {interval.hours} hores - {interval.minutes} minuts{" "}
              </h4>
              <Espai />
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default DatesF;
